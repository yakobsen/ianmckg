package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        /***************/

        // Error if -- (Will be replaced to + if occurs)
        
        try {
            if (statement.contains("--"))
                return null;
            while (statement.contains(")")) {
                int closeBracket = statement.indexOf(")");
                int openBracket = -1;
                for (int i = 0; i < closeBracket; i++) {
                    if (statement.charAt(i) == '(') {
                        openBracket = i;
                    }
                }
                String localResult = Calculator.calculateInBrackets(statement.substring(openBracket + 1, closeBracket));

                StringBuilder sb = new StringBuilder();
                if (openBracket != 0) {
                    sb.append(statement.substring(0, openBracket));
                }
                sb.append(localResult);
                if (closeBracket != statement.length() - 1) {
                    sb.append(statement.substring(closeBracket + 1, statement.length()));
                }
                statement = sb.toString();
            }

            statement = calculateInBrackets(statement).replace("_", "-");
            double resultD = Double.parseDouble(statement);
            if (resultD % 1 == 0) {
                int resultInt = (int) resultD;
                statement = resultInt + "";
            }
            return statement;
        } catch (Exception e) {
            return null;
        }
    }
    private static String calculateInBrackets(String statement) {
        //System.out.println("Local in brackets: " + statement);
        while (statement.contains("/")) {
            //System.out.println("BEFORE: " + statement);
            statement = Calculator.operatorEvaluate(statement, "/");
            //System.out.println("AFTER: " + statement);
        }
        while (statement.contains("*")) {
            //System.out.println("BEFORE: " + statement);
            statement = Calculator.operatorEvaluate(statement, "*");
            //System.out.println("AFTER: " + statement);
        }
        while (statement.contains("-")) {
            //System.out.println("BEFORE: " + statement);
            statement = Calculator.operatorEvaluate(statement, "+");
            //System.out.println("AFTER: " + statement);
        }
        while (statement.contains("+")) {
            //System.out.println("BEFORE: " + statement);
            statement = Calculator.operatorEvaluate(statement, "+");
            //System.out.println("AFTER: " + statement);
        }
        return statement;
    }
    private static String operatorEvaluate(String statement, String operator) {

        // Replace seq
        statement = statement.replaceAll("\\*-", "\\*_");
        statement = statement.replaceAll("/-", "/_");
        statement = statement.replaceAll("\\+-", "\\+_");
        statement = statement.replaceAll("-", "\\+_");

      //  System.out.println(statement);

        // First minus is not operator
        if (statement.charAt(0) == '-') {
            statement = "_" + statement.substring(1);
            //System.out.println("New statement = " + statement);
        }

        if (statement.charAt(0) == '+') {
            statement = statement.substring(1);
            //System.out.println("New statement = " + statement);
        }

       // System.out.println(statement);

        if (!(statement.contains(operator))) return statement;

        int operatorIndex = statement.indexOf(operator);
        // Find index of begin and end of the first value
        int val1Index1 = 0;
        int val1Index2 = operatorIndex-1;
        for (int i = 0; i < operatorIndex; i++) {
            if (Calculator.isOperator(statement.charAt(i))) {
                val1Index1 = i+1;
            }
        }
        // Find index of begin and end of the second value
        int val2Index1 = operatorIndex+1;
        int val2Index2 = statement.length() - 1;
        for (int i = operatorIndex + 1; i < statement.length(); i++) {
            if (Calculator.isOperator(statement.charAt(i))) {
                val2Index2 = i-1;
                break;
            }
        }

        String val1 = statement.substring(val1Index1,val1Index2 + 1);
        String val2 = statement.substring(val2Index1,val2Index2 + 1);
        //System.out.println(val1 + " and " + statement.charAt(operatorIndex) + " and " + val2);
        String localResult = Calculator.binarEvaluate(val1, statement.charAt(operatorIndex), val2);
        StringBuilder sb = new StringBuilder();


        if (val1Index1 != 0) {
            sb.append(statement.substring(0, val1Index1));
            String leftPart = "left: " + statement.substring(0, val1Index1);
            //System.out.println(leftPart);
        }
        sb.append(localResult);
        if (val2Index2 != statement.length() - 1) {
            sb.append(statement.substring(val2Index2 + 1, statement.length()));
            String rightPart = "Right: " + statement.substring(val2Index2 + 1, statement.length());
            //System.out.println(rightPart);
        }
        statement = sb.toString();
        return statement;
    }
    private static boolean isOperator (char c) {
        switch (c) {
            case ('+'):
            case ('-'):
            case ('*'):
            case ('/'):
                return true;
            default: return false;
        }
    }
    private static String binarEvaluate(String val1, char operator, String val2) {
        double val1D = Double.parseDouble(val1.replace("_","-"));
        double val2D = Double.parseDouble(val2.replace("_","-"));

        double result;

        switch (operator) {
            case ('+'):
                result = val1D + val2D;
                break;
            case ('-'):
                result = val1D - val2D;
                break;
            case ('*'):
                result = val1D * val2D;
                break;
            case ('/'):
                if (val2D == 0) {
                    return null;
                } else {
                    result = val1D / val2D;
                }
                break;
            default:
                return null;
        }
        return Double.toString(result).replace("_","-");
        /***************/
    }
}
