package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Check of amount of element 
        /*
        int elementCounter = 0;
        int rowsCounter = 0;
        while (elementCounter < inputNumbers.size()) {
            elementCounter += ++rowsCounter;
        }

        if (elementCounter != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }

        // Sort and catch null
        try {
            for (int i = 0; i <= elementCounter; i++) {
                for (int j = 0; j < elementCounter - 1; j++) {
                    if (inputNumbers.get(j) > inputNumbers.get(j + 1)) {
                        int temp = inputNumbers.get(j);
                        inputNumbers.set(j, inputNumbers.get(j + 1));
                        inputNumbers.set(j + 1, temp);
                    }
                }
            }
        } catch (RuntimeException e) {
            throw new CannotBuildPyramidException();
        }
        // Pyramid 
        int width = rowsCounter * 2 - 1;
        int[][] pyramid = new int[rowsCounter][width];
        // Fill with 0

        for (int r = 0; r < rowsCounter; r++) {
            for (int c = 0; c < width; c++) {
                pyramid[r][c] = 0;
            }
        }

        int indexOfElemnt = 0;
        for (int r = 0; r < rowsCounter; r++) {
            boolean rowStarted = false;
            boolean notEmptyIndex = true;
            int elementsInRow = 0;


            for (int c = 0; c < width; c++) {
                if (!rowStarted) {
                    if (c == rowsCounter - r - 1) {
                        rowStarted = true;
                    }
                }

                if (rowStarted) {
                    if (elementsInRow < r + 1) {
                        if (notEmptyIndex) {
                            pyramid[r][c] = inputNumbers.get(indexOfElemnt++);
                            elementsInRow++;
                            notEmptyIndex = false;
                        } else {
                            notEmptyIndex = true;
                        }
                    }
                }
            }
        }
        return pyramid;
        */
        return new int[1][1];
    }
}
