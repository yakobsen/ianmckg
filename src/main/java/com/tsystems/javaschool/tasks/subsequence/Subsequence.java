package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        try {
            int yIndex = 0;
            int xIndex = 0;

            while (xIndex < x.size() && yIndex < y.size()) {
                if (y.get(yIndex).equals(x.get(xIndex))) {
                    xIndex++;
                    yIndex++;
                } else {
                    y.remove(yIndex);
                }
                if (x.size() > y.size()) return false;
            }

            if (x.size() > y.size()) return false;

            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
